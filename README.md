# SIFAC LEDs with Pro Micro and WS2812B

A quick way to get LED data from ll3.exe to a microcontroller while a proper IO isn't figured out yet.

## WS2812B LED assignment
This sample uses 14 LEDs and assigns them as the following:\
LED 0: Cancel Button Light\
LEDs 1-9: Rhythm Button Lights\
LED 10: Confirm Button Light\
LED 11,12,13: Lightstick Top, Middle, Bottom Lights

## Wiring
| Pro Micro | WS2812B |
|  ---  | --- |
| Vcc | +5v | 
| A2 | D in | 
| GND | GND | 

## Python Dependencies
Pymem:\
pip install pymem

Pyserial:\
pip install pyserial

## Pro Micro Dependencies
FastLED:\
If using Arduino IDE:\
https://docs.arduino.cc/software/ide-v1/tutorials/installing-libraries

If via PlatformIO in VSCode:\
https://platformio.org/install/ide?install=vscode \
then\
PIO Home > Libraries > Type "Fastled" in Search libraries > Click on FastLED > Add to Project > Select your Project > Add

## Instructions
1. Wire WS2812B to Pro Micro.
2. Build and Upload main.cpp to Pro Micro.
3. Set proper COM Port of Pro Micro in SIFAC_Leds.py.
4. Run ll3.exe.
5. Run SIFAC_Leds.py.

## Memory Details
When using jconfig, LED data is being written to base address of ll3.exe plus offset 0xbbb50c.\
Or in Cheat Engine Memory View: ll3.exe + 0xbbb50c.

The same LED data is found on ll3.exe + 0xbbb44c.

Comprises 24 bytes corresponding to the following:
| Bytes | LED |
|  ---  | --- |
|0 |    Lightstick Top Red|
|1  |   Lightstick Top Green|
|2  |   Lightstick Top Blue|
|3  |   Lightstick Middle Red|
|4  |   Lightstick Middle Green|
|5  |   Lightstick Middle Blue|
|6  |   Lightstick Bottom Red|
|7  |   Lightstick Bottom Green|
|8  |   Lightstick Bottom Blue|
|9  |   Character Color Red|
|10 |   Character Color Green|
|11  |  Character Color Blue|
|12  |  Rhythm Button 1 (Leftmost)|
|13  |  Rhythm Button 2|
|14-15 |Unused|
|16-21 |Rhythm Buttons 3-8|
|22  |  Rhythm Buttons 9 (Rightmost)|
|23  |  Cancel and Confirm Buttons|

[TR,TG,TB, MR,MG,MB, BR,BG,BB, CR,CG,CB, R1,R2, X,X, R3,R4,R5,R6,R7,R8,R9, CC]