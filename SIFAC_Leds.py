from pymem import Pymem
import serial

#Set COM port of Arduino Pro Micro here
COM_PORT = 'COM7'

ser = serial.Serial(COM_PORT, 9600, timeout=0, parity=serial.PARITY_EVEN, rtscts=1)

pm = Pymem('ll3.exe')
print('Process id: %s' % pm.process_id)
targetAddress = pm.base_address + int('bbb50c',16)
while True:
    read_bytes = pm.read_bytes(targetAddress,24)
    ser.write(read_bytes)
