//SIFAC Leds for Arduino Pro Micro
//Obtains light data through SIFAC_Leds.py via serial.

#include <Arduino.h>
#include <fastled.h>

//FASTLED declarations
#define NUM_LEDS 14
#define DATA_PIN 20
CRGB leds[NUM_LEDS];
int led_brightness = 5;

//serial declarations
const int BUFFER_SIZE = 24;
byte buf[BUFFER_SIZE];

int RXLED = 17; 

void setup() {
  pinMode(RXLED, OUTPUT);
  digitalWrite(RXLED, HIGH); // turn off RX LED

  FastLED.addLeds<WS2812B, DATA_PIN, GRB>(leds, NUM_LEDS); //LED type declaration
  FastLED.setBrightness(led_brightness);
  
  FastLED.clear();  // clear all pixel data
  FastLED.show();

  leds[0] = 0xFFFFFF; //turn first LED white to check if upload ok
  FastLED.show();

  Serial.begin(9600); //Start serial communication
}
void loop() {
  if (Serial.available() > 0) {
    //read 24 bytes from serial 
    int rlen = Serial.readBytes(buf, BUFFER_SIZE);

    //Lightsticks
    for (int i = 0; i <= 2; i++) {
      leds[i+11].setRGB(buf[i*3],buf[i*3+1],buf[i*3+2]);
    }
    //R1 & R2
    for (int i = 0; i <= 1; i++){
      leds[i+1].setRGB(buf[i + 12],buf[i + 12],buf[i + 12]);
    }
    //R3 - R9
    for (int i = 0; i <= 6; i++){
      leds[i+3].setRGB(buf[i + 16],buf[i + 16],buf[i + 16]);
    }

    leds[0].setRGB(buf[23],0,0); //cancel button
    leds[10].setRGB(0,0,buf[23]); //confirm button

    FastLED.show();
  }
}
